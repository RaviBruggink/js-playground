var follower = document.getElementById('ball');
var followerWidth = follower.offsetWidth;
var followerHeight = follower.offsetHeight;

document.addEventListener('mousemove', function(e) {
  setTimeout(function() {
    var x = e.clientX - (followerWidth / 2);
    var y = e.clientY - (followerHeight / 2);
    follower.style.left = x + 'px';
    follower.style.top = y + 'px';
  }, 20);
});

document.addEventListener('scroll', function(e) {
  setTimeout(function() {
    var x = e.clientX - (followerWidth / 2);
    var y = e.clientY - (followerHeight / 2);
    follower.style.left = x + 'px';
    follower.style.top = y + 'px';
  }, 20);
});

const text = document.querySelectorAll("div.text");
//makes a variable from the contents of all text in the class "text" AND only when they are a div

text.forEach(function(element) {
    let tick = 1;
    let value = element.dataset.speed;
    element.innerHTML += element.innerHTML;
    element.innerHTML += element.innerHTML;

    const innerTags = element.querySelectorAll("div.inner");
    innerTags.forEach((inner, index) => {
        inner.style.left = inner.offsetWidth * index + "px";
    });
    //For each "inner" element the code sets the "left" CSS property to the width of the element multiplied by "index".

    const ticker = function() {
        tick += parseInt(value);
        //element.innerHTML = tick;
        //element.style.left = tick + "px";
        innerTags.forEach((inner, index) => {
            let width = inner.offsetWidth;
            let normalizedTextX = ((tick % width) + width) % width;
            let pos = width * (index - 1) + normalizedTextX;

            inner.style.left = pos + "px";
        });
        requestAnimationFrame(ticker);
        //Causes "ticker" to be initiated again creating a loop
    };
    ticker();
});